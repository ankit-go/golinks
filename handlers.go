package main

import (
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"github.com/syndtr/goleveldb/leveldb"
	dbError "github.com/syndtr/goleveldb/leveldb/errors"
)

func newRootRedirectHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Debug().Msgf("redirect root to %s", rootRedirect)
		http.Redirect(w, r, rootRedirect, http.StatusTemporaryRedirect)
	})
}

func newLinkRedirectHandler(db *leveldb.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)

		slug, ok := vars["slug"]
		if !ok {
			log.Debug().Msg("slug not found in the request")
			w.WriteHeader(http.StatusBadRequest)
			errTmpl.Execute(w, "invalid request: slug not found")
			return
		}

		value, err := db.Get([]byte(slug), nil)
		if err != nil {
			if err.Error() == dbError.ErrNotFound.Error() {
				http.Redirect(w, r, "/edit/"+slug, http.StatusTemporaryRedirect)
			} else {
				log.Error().AnErr("leveldb error", err).Msgf("failed to get %s from database", slug)
				w.WriteHeader(http.StatusInternalServerError)
				errTmpl.Execute(w, "internal server error")
			}
			return
		}

		log.Debug().Str("slug", slug).Msgf("redirect to %s", string(value))
		http.Redirect(w, r, string(value), http.StatusTemporaryRedirect)
	})
}

func newEditGetHandler(db *leveldb.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)

		slug, ok := vars["slug"]
		if !ok {
			log.Debug().Msg("slug not found in the request, setting empty string")
			slug = ""
		}

		value, err := db.Get([]byte(slug), nil)
		if err != nil {
			if err.Error() == dbError.ErrNotFound.Error() {
				log.Debug().Msgf("slug %s not found in database, setting empty string", slug)
				value = []byte("")
			} else {
				log.Error().AnErr("leveldb error", err).Msgf("failed to get %s from database", slug)
				w.WriteHeader(http.StatusInternalServerError)
				errTmpl.Execute(w, "internal server error")
				return
			}
		}

		editTmpl.Execute(w, &templateValues{Slug: slug, Value: string(value), Creation: true})
	})
}

func newEditPostHandler(db *leveldb.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		slug := r.FormValue("slug")
		if slug == "" {
			log.Debug().Msg("slug not found in the request")
			w.WriteHeader(http.StatusBadRequest)
			errTmpl.Execute(w, "invalid request: name not found")
			return
		}
		save := r.FormValue("save")
		delete := r.FormValue("delete")

		if save == "true" {
			value := r.FormValue("value")
			if value == "" {
				log.Debug().Msgf("no value was given for the slug %s", slug)
				w.WriteHeader(http.StatusBadRequest)
				errTmpl.Execute(w, "invalid request: url not found")
				return
			}
			_, err := url.ParseRequestURI(value)
			if err != nil {
				log.Debug().Msgf("invalid value %s was passed", value)
				w.WriteHeader(http.StatusBadRequest)
				errTmpl.Execute(w, "invalid request: passed url is not valid")
				return
			}

			err = db.Put([]byte(slug), []byte(value), nil)
			if err != nil {
				log.Error().AnErr("leveldb error", err).Msgf("failed to put %s : %s from database", slug, value)
				w.WriteHeader(http.StatusInternalServerError)
				errTmpl.Execute(w, "internal server error")
				return
			}

			log.Debug().Str(slug, value).Msg("successfully edited the entry")
			successTmpl.Execute(w, &templateValues{Slug: slug, Value: value, Creation: true})
		} else if delete == "true" {
			err := db.Delete([]byte(slug), nil)
			if err != nil {
				log.Error().AnErr("leveldb error", err).Msgf("failed to delete %s from database", slug)
				w.WriteHeader(http.StatusInternalServerError)
				errTmpl.Execute(w, "internal server error")
				return
			}

			log.Debug().Msgf("successfully deleted the entry %s", slug)
			successTmpl.Execute(w, &templateValues{Slug: slug, Deletion: true})
		} else {
			http.Redirect(w, r, "/edit/"+slug, http.StatusTemporaryRedirect)
		}
		return
	})
}

func newLinksHandler(db *leveldb.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var links []templateValues
		iter := db.NewIterator(nil, nil)
		for iter.Next() {
			key := iter.Key()
			value := iter.Value()
			links = append(links, templateValues{Slug: string(key), Value: string(value)})
		}
		iter.Release()

		linksTmpl.Execute(w, links)
	})
}
