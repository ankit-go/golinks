package main

import (
	"github.com/rs/zerolog/log"
	"net/http"
	"time"
)

func loggingMiddleWare(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func(t time.Time) {
			log.Info().Str(r.Method, r.URL.Path).Msgf("duration: %s", time.Since(t))
		}(time.Now())
		h.ServeHTTP(w, r)
	})
}

func authenticationMiddleWare(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user := r.Header.Get("REMOTE_USER")
		if user == "" {
			log.Info().Msg("unauthentication request terminated")
			errTmpl.Execute(w, "not authorized to access this page")
			return
		}
		h.ServeHTTP(w, r)
	})
}
