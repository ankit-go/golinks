package main

import (
	"flag"
	"html/template"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/syndtr/goleveldb/leveldb"
)

var (
	websiteHost  string
	host         string
	port         int
	addr         string
	rootRedirect string
	tmplDir      string
	develop      bool
	auth         bool

	errTmpl     *template.Template
	successTmpl *template.Template
	editTmpl    *template.Template
	linksTmpl   *template.Template
)

func initialize() {
	// Parse command-line flags
	flag.StringVar(&websiteHost, "website", "", "The FDQN of the host. If left empty it will listen to requests for any host.")
	flag.StringVar(&host, "host", "0.0.0.0", "The host on which web server will listen.")
	flag.StringVar(&rootRedirect, "root", "/edit", "The url where where root page will redirect. If left empty, it will redirect to /edit")
	flag.StringVar(&tmplDir, "templates", "templates/", "Directory to use custom templates")
	flag.BoolVar(&develop, "develop", false, "Run golinks in develop mode")
	flag.BoolVar(&auth, "auth", false, "Enable authentication for edit endpoints. Note that golinks do not implement the authentication, instead it checks REMOTE_USER header only")
	flag.IntVar(&port, "port", 9876, "The post on which web server will listen.")
	flag.Parse()

	addr = host + ":" + strconv.Itoa(port)

	// Setup logger
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	zerolog.TimeFieldFormat = time.RFC3339
	if develop {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	// Parse all the templates
	var err error
	editTmpl, err = template.ParseFiles("./templates/edit.tmpl")
	if err != nil {
		log.Fatal().AnErr("template error", err).Msg("failed to parse edit template")
	}
	successTmpl, err = template.ParseFiles("./templates/success.tmpl")
	if err != nil {
		log.Fatal().AnErr("template error", err).Msg("failed to parse success template")
	}
	errTmpl, err = template.ParseFiles("./templates/error.tmpl")
	if err != nil {
		log.Fatal().AnErr("template error", err).Msg("failed to parse error template")
	}
	linksTmpl, err = template.ParseFiles("./templates/links.tmpl")
	if err != nil {
		log.Fatal().AnErr("template error", err).Msg("failed to parse links template")
	}
}

func main() {
	initialize()

	// Initialize leveldb. The pointer is concurrency safe and can be
	// passed everywhere.
	db, err := leveldb.OpenFile("data", nil)
	if err != nil {
		log.Fatal().AnErr("leveldb error", err).Msg("failed to open database")
	}

	// Create new router for the golinks routes
	r := mux.NewRouter()

	// If website host is given, restrict the router to only serve
	// website host requests.
	if websiteHost != "" {
		r.Host(websiteHost)
	}

	// Generate handlers for the endpoints
	editGetHandler := loggingMiddleWare(newEditGetHandler(db))
	editPostHandler := loggingMiddleWare(newEditPostHandler(db))
	linkRedirectHandler := loggingMiddleWare(newLinkRedirectHandler(db))
	linksHandler := loggingMiddleWare(newLinksHandler(db))
	rootRedirectHandler := loggingMiddleWare(newRootRedirectHandler())

	// Add authentication middleware, if authentication is enabled.
	if auth {
		editGetHandler = authenticationMiddleWare(editGetHandler)
		editPostHandler = authenticationMiddleWare(editPostHandler)
		linksHandler = authenticationMiddleWare(linksHandler)
	}

	// Register handlers for all the routes
	r.Handle("/edit", editGetHandler).Methods("GET")
	r.Handle("/edit/{slug}", editGetHandler).Methods("GET")
	r.Handle("/edit", editPostHandler).Methods("POST")
	r.Handle("/links", linksHandler).Methods("GET")
	r.Handle("/{slug}", linkRedirectHandler).Methods("GET")
	r.Handle("/", rootRedirectHandler).Methods("GET")

	// Start the web server
	log.Info().Msgf("Listening on %s", addr)
	http.ListenAndServe(addr, r)
}
