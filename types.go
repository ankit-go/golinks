package main

type templateValues struct {
	Slug     string
	Value    string
	Creation bool
	Deletion bool
}
