module argp.in/golinks

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/kr/pretty v0.1.0 // indirect
	github.com/rs/zerolog v1.17.2
	github.com/syndtr/goleveldb v1.0.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
