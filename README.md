# Golinks

I decided to do this project on the weekend to learn about Mux. This
project is inspired by Google's internal "go links".

This project is written in Golang so it can be cross-compiled to
various operating systems and architectures. It uses embedded leveldb
for storing information so there is no need to setup database. It can
be compiled by running the following command. It will generate

```bash
$ go build -o golinks *.go
```

This will generate `golinks` executable binary which can be installed
and controlled using Systemd service. For options run `golinks
--help`.
